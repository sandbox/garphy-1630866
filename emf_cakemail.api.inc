<?php
/**
 * @file
 * CakeMail API call wrappers.
 */

/**
 * Common helper for API services invocation.
 *
 * @param string $service
 *   Service name.
 * @param string $method
 *   Method name.
 * @param array $data
 *   Method arguments.
 *
 * @return bool|mixed
 *   The services return or FALSE in case of errors.
 */
function _emf_cakemail_api_invoke($service, $method, $data) {

  $api_key = _emf_cakemail_api_get_api_key();

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'https://api.wbsrvc.com/' . $service . '/' . $method);
  curl_setopt($ch, CURLOPT_POST, TRUE);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('apikey: ' . $api_key));
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

  $result = curl_exec($ch);

  if ($result === FALSE) {
    unset($result);
    watchdog('emf cakemail', curl_error($ch), array(), WATCHDOG_ERROR);
  }
  curl_close($ch);

  if (isset($result)) {
    $json_object = json_decode($result, TRUE);
    return $json_object;
  }

  return FALSE;

}

/**
 * Retrieve the API Key.
 *
 * @return string
 *   The configured API Key.
 */
function _emf_cakemail_api_get_api_key() {
  return variable_get('emf_cakemail_api_key', '');
}

/**
 * Retrieve the Client ID.
 *
 * @return string
 *   The configured Client ID.
 */
function _emf_cakemail_api_get_client_id() {
  return variable_get('emf_cakemail_client_id', '');
}

/**
 * Helper function to build the method data array with shared common attributes.
 *
 * @return array
 *   An array prefilled with 'user_key' & 'client_id'.
 */
function _emf_cakemail_api_stub_data() {
  return array('user_key' => _emf_cakemail_api_get_user_key(), 'client_id' => _emf_cakemail_api_get_client_id());
}

/**
 * Return the API user key, performing a login on the API if needed.
 *
 * @return string
 *   The current user key.
 */
function _emf_cakemail_api_get_user_key() {

  static $cakemail_api_user_key;

  if (!isset($cakemail_api_user_key)) {

    $client_id = _emf_cakemail_api_get_client_id();

    $data = array(
      'email' => variable_get('emf_cakemail_user_email', ''),
      'password' => variable_get('emf_cakemail_user_password', ''),
      'client_id' => $client_id,
    );

    $return = _emf_cakemail_api_invoke('User', 'Login', $data);

    if (is_array($return) && $return['status'] == 'success') {
      $cakemail_api_user_key = $return['data']['user_key'];
    }

  }

  return $cakemail_api_user_key;

}


/**
 * Subscribe a user to a list.
 *
 * @param string $email
 *   E-mail address to subscribe.
 * @param array $fields
 *   Array of custom field values. Key is field. Value is value for the field.
 * @param string $lid
 *   List ID of the list to subscribe to.
 *
 * @return boolean
 *   TRUE if user is subscribed. FALSE if not.
 */
function emf_cakemail_api_subscribe($email, $fields, $lid) {
  $data = _emf_cakemail_api_stub_data() + array(
    'list_id' => $lid,
    'email' => $email,
    'data' => $fields,
  );

  $return = _emf_cakemail_api_invoke("List", "SubscribeEmail", $data);

  return is_array($return) && $return['status'] == 'success';
}

/**
 * Unsubscribe a user from a list.
 *
 * @param string $email
 *   E-mail address to subscribe.
 * @param string $lid
 *   List ID of the list to subscribe to.
 *
 * @return boolean
 *   TRUE if user is subscribed. FALSE if not.
 */
function emf_cakemail_api_unsubscribe($email, $lid) {
  $data = _emf_cakemail_api_stub_data() + array(
    'list_id' => $lid,
    'email' => $email,
  );

  $return = _emf_cakemail_api_invoke("List", "UnsubscribeEmail", $data);

  return is_array($return) && $return['status'] == 'success';
}


/**
 * Fetch subscribed subscribers from API.
 *
 * @param mixed $date
 *   If a string, should be in the date() format of 'Y-m-d H:i:s', otherwise,
 *   a Unix timestamp.
 * @param string $lid
 *   List ID
 *
 * @return array
 *   List of subscriber.
 */
function emf_cakemail_api_get_subscribers_subscribed($date = 0, $lid = NULL) {

  $data = _emf_cakemail_api_stub_data() + array(
    'list_id' => $lid,
    'status' => 'active',
  );

  $return = _emf_cakemail_api_invoke("List", "Show", $data);

  if (is_array($return) && $return['status'] == 'success') {

    $mails = array();

    foreach ($return['data']['records'] as $record) {
      $mails[] = $record['email'];
    }

    return $mails;

  }

  return FALSE;
}


/**
 * Fetch unsubscribed subscribers from API.
 *
 * @param mixed $date
 *   If a string, should be in the date() format of 'Y-m-d H:i:s', otherwise,
 *   a Unix timestamp.
 * @param string $lid
 *   List ID
 *
 * @return array
 *   List of unsubscribed subscriber.
 */
function emf_cakemail_api_get_subscribers_unsubscribed($date = 0, $lid = NULL) {

  $data = _emf_cakemail_api_stub_data() + array(
    'list_id' => $lid,
    'status' => 'unsubscribed',
  );

  $return = _emf_cakemail_api_invoke("List", "Show", $data);

  if (is_array($return) && $return['status'] == 'success') {

    $mails = array();

    foreach ($return['data']['records'] as $record) {
      $mails[] = $record['email'];
    }

    return $mails;

  }

  return FALSE;

}


/**
 * Fetch lists from API.
 *
 * @return array
 *   List of available lists.
 */
function emf_cakemail_api_get_lists() {

  $lists = array();

  $data = _emf_cakemail_api_stub_data();

  $return = _emf_cakemail_api_invoke('List', 'GetList', $data);

  if (is_array($return) && $return['status'] == 'success') {
    foreach ($return['data']['lists'] as $list_api) {

      $list = (object) array(
        'lid' => $list_api['id'],
        'name_api' => $list_api['name'],
        'status_api' => $list_api['status'] == 'active' ? TRUE : FALSE,
      );

      $lists[$list->lid] = $list;

    }
  }

  return $lists;
}

/**
 * Fetch custom fields for some list from API.
 *
 * @param string $lid
 *   List ID of the list.
 *
 * @return array
 *   List of custom fields.
 */
function emf_cakemail_api_get_custom_fields($lid) {

  $data = _emf_cakemail_api_stub_data();
  $data += array(
    'list_id' => $lid,
  );

  $field_type_mapping = array(
    'integer' => 'text',
    'text' => 'text',
    'timestamp' => 'text',
  );

  $return = _emf_cakemail_api_invoke('List', 'GetFields', $data);

  if (is_array($return) && $return['status'] == 'success') {

    foreach ($return['data'] as $key => $type) {

      if (in_array($key, array('id', 'registered'))) {
        continue;
      }

      if ($key == 'email') {
        $key = 'mail';
      }

      $fields[$key] = (object) array(
        'key' => $key,
        'name' => drupal_ucfirst($key),
        'type' => $field_type_mapping[$type],
      );
    }

  }

  return $fields;

}

/**
 * Convert a UNIX timestamp to a CakeMail date/time.
 *
 * @param int $timestamp
 *   The UNIX timestamp to convert.
 *
 * @return string
 *   The Date in CakeMail format.
 */
function emf_cakemail_api_unix_to_service_time($timestamp = 0) {
  // CakeMail expects this format : 2012-06-12 23:54:13.
  return date('Y-m-d H:i:s', $timestamp);
}
