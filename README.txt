Description
-----------

EMF CakeMail extends the Email Marketing Framework module to support the
CakeMail mass mailing platform and maintain synchronized subscriber lists
with Drupal..

It requires a working install of the Email Marketing Framework and a
CakeMail API access.

See the project page at http://drupal.org/sandbox/garphy/1630866


Installation
------------

Install the EMF CakeMail module as usual. There's no required 3rd-party
library for the module to operate.

Dependencies
------------

The EMF CakeMail module is dependent on the EMF module.

Configuration
-------------

The configuration page is at admin/config/emf/cakemail, where you can enter
your API details.

Available lists will appear in EMF admin area once cron has run.
