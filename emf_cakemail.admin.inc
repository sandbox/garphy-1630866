<?php

/**
 * @file
 * CakeMail Plugin settings management forms
 */


/**
 * CakeMail API settings form definition.
 */
function emf_cakemail_settings_form() {
  $form = array();

  $form['emf_cakemail_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#description' => t('Your CakeMail API Key. See CakeMail\'s <a href="http://dev.cakemail.com/api/">documentation</a> for more info.'),
    '#default_value' => variable_get('emf_cakemail_api_key', ''),
    '#required' => TRUE,
  );
  $form['emf_cakemail_client_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID'),
    '#description' => t('Your CakeMail Client ID. See CakeMail\'s <a href="http://dev.cakemail.com/api/">documentation</a> for more info.'),
    '#default_value' => variable_get('emf_cakemail_client_id', ''),
    '#required' => TRUE,
  );
  $form['emf_cakemail_user_email'] = array(
    '#type' => 'textfield',
    '#title' => t('User email (login)'),
    '#description' => t('Your CakeMail User login email. See CakeMail\'s <a href="http://dev.cakemail.com/api/">documentation</a> for more info.'),
    '#default_value' => variable_get('emf_cakemail_user_email', ''),
    '#required' => TRUE,
  );
  $form['emf_cakemail_user_password'] = array(
    '#type' => 'password',
    '#title' => t('User password'),
    '#description' => t('Your CakeMail User password. See CakeMail\'s <a href="http://dev.cakemail.com/api/">documentation</a> for more info.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
